# Documentation of the project

## [PL] Projekt i wykonanie automatycznych testów GitLaB z wykorzystaniem SoapUI.docx (*.pdf)
Those documents are documentation of the project and master's thesis of the author.  
Available only in Polish.  
  
## [ENG] API - suggestions.txt
Describes how to get ID of suggestion (PUT https://gitlab.com/api/v4/suggestions/:id/apply).  
Credits go to aljaxus from this thread: https://forum.gitlab.com/t/applying-suggestions-where-to-get-id/36864/4.  
  
## [PL] notes_api.txt
Just some author's notes made during creation of the framework.  
  
## [PL] notes_docker.txt
Notes on how to create docker to test **/projects/:id/registry/repositories**.  
  
## [PL] notes_other.txt
Was meant for other notes, but in the end describes only one way of sending files through POST in SoapUI.