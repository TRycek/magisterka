# Automatic tests design and development based on GitLab using SoapUI

The project is my master thesis written on West Pomeranian University of Technology under Luiza Fabisiak, DEng.

## Abstract

The project aims to create so far non-existent public test framework, designed to automate tests of the REST API, which is provided by the website called GitLab, that provides services related to the software development process. The framework will be created using SoapUI and then integrated with JUnit 5 and Maven. In addition, a sample test suite will be created, designed from the point of view of a non-premium user of the service.  

**Key words:** GitLab, SoapUI, automatic tests, REST, Maven, JUnit5, API.  
  
**API:** https://docs.gitlab.com/ee/api/api_resources.html  
  
**Authorization endpoints:** https://gitlab.com/.well-known/openid-configuration

## Content

The thesis is mostly in Polish, but there are some additional English descriptions in **README** files of directories. If something is unclear or not described in those files, feel free to contact me via [mail](mailto:trycek08@gmail.com) or [LinkedIN](https://www.linkedin.com/in/jacek-wolny/).

### GitLabAPITests.xml

Main file, includes test framework and sample test suite.  
**Framework** covers 387 end points (EPs) of GitLab's REST API v4. The list can be found at the end of thesis (page 107).  
Since this version of the API was not finished at the moment, I can confirm covering:

- all EPs present in documentation till 23.04.2020 (that's when I started),
- some or all EPs added from 23.04.2020 to 09.05.2020 (depends when it was added and where in the documentation),
- and none added after 09.05.2020.

**Sample test suite** consists of 40 test cases and covers 153 EPs (mainly _projects_ and it's children). The list can be found at the end of thesis (page 101).  
It is designed from the perspective of regular non-premium user. In addition, only so called "happy paths" are covered.  

### **GitLabAPITestsExample** directory

Java project showing integration of framework with JUnit 5. Also briefly described in directorie's README.

### **docs** directory

The thesis itself plus some other personal notes.

### **files** directory

Just some rabbish downloaded/uploaded in REST requests.

### **maven** directory

Configuration file and description for framework integration with Maven.