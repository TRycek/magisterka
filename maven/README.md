# Integration with Maven

The test framework can be integrated with Maven to run whole test suites or particular test cases right from Maven.  
Just perform below steps (more description in further chapters):

1. Create and configure pom.xml file.
1. If you want to run only particular test suites/cases, set it in pom.xml.
1. Run tests via Maven.

## Configuration

In below pom.xml:

- change `<projectFile>` node to point at GitLabAPITests.xml (or place the file in the same directory as pom.xml),
- set project properties in subnodes of `<projectProperties>`,

## Running tests

From system console, while being in the same directory as pom.xml, run:  
```
mvn com.smartbear.soapui:soapui-maven-plugin:5.6.0:test
```
  
By default, it will run all test cases in the `<projectFile>`, unless set otherwise.

## Choosing test suite or test case

To run particular test suite, add new `<testSuite>` node inside of `<configuration>` node:
```xml
<testSuite>APITestSuite_1</testSuite>
```

To run particular test case, add new `<testSuite>` node inside of `<configuration>` node:
```xml
<testCase>[TC001] avatar TestCase</testCase>
```

Of course you can mix them and add more than one.

## pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

    <repositories>
        <repository>
            <id>SmartBearPluginRepository</id>
            <url>http://www.soapui.org/repository/maven2/</url>
        </repository>
    </repositories>

    <parent>
        <groupId>com.smartbear.soapui</groupId>
        <artifactId>soapui-project</artifactId>
        <version>5.6.0</version>
    </parent>

    <artifactId>soapui-maven-plugin</artifactId>
    <name>SoapUI Maven plugin</name>
    <description> ( ͡° ͜ʖ ͡°) </description>
    <packaging>maven-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.smartbear.soapui</groupId>
            <artifactId>soapui</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.smartbear.soapui</groupId>
                <artifactId>soapui-maven-plugin</artifactId>
                <version>5.6.0</version>
                <configuration>
                    <projectFile>GitLabAPITests.xml</projectFile>
                    <projectProperties>
                        <value>testUser=JWMagisterka</value>
                        <value>testPass=JWMgr2020</value>
                        <value>testEmail=jacekwolny8@wp.pl</value>
                        <value>personalAccessToken=K7rfUfcwbsAf3DgdpU</value>
                    </projectProperties>
                    <outputFolder>./maven_output_errors</outputFolder>
                </configuration>
                <executions>
                    <execution>
                        <phase>test</phase>
                        <goals>
                            <goal>test</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```
