# Integration with Maven and JUnit 5

[[_TOC_]]

The test framework can be integrated with Maven and JUnit 5 to run or create test cases.

1. Create Java project.
1. Create and configure pom.xml file.
1. Create test class.
1. Run tests created in SoapUI or create and run new tests.

## Maven (pom.xml) configuration

Similar as when using just Maven, but this time without whole `<build>` node and with dependency for JUnit 5.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

    <repositories>
        <repository>
            <id>SmartBearPluginRepository</id>
            <url>http://www.soapui.org/repository/maven2/</url>
        </repository>
    </repositories>

    <parent>
        <groupId>com.smartbear.soapui</groupId>
        <artifactId>soapui-project</artifactId>
        <version>5.6.0</version>
    </parent>

    <artifactId>soapui-maven-plugin</artifactId>
    <name>SoapUI Maven plugin</name>
    <description> (╯°□°）╯︵ ┻━┻ </description>
    <packaging>maven-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.smartbear.soapui</groupId>
            <artifactId>soapui</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>5.6.2</version>
        </dependency>
    </dependencies>
</project>
```

## Run all tests

To run all tests created in SoapUI, just create runner.

```java
import com.eviware.soapui.tools.SoapUITestCaseRunner;
import org.junit.jupiter.api.Test;

public class WholeSoapUITestSuite {
    @Test
    public void runAllTests() throws Exception
    {
        SoapUITestCaseRunner runner = new SoapUITestCaseRunner();
        runner.setProjectFile("GitLabAPITests.xml");
        runner.run();
    }
}
```

## Run particular test case

To run particular test case created in SoapUI, you have to first get test suite. Then get desired test case and run it.  
You could also run whole test suite, since `TestSuite` class also offers `run()` method.

```java
@Test
public void TC001_avatarTestCase() throws Exception
{
    WsdlProject project = new WsdlProject("GitLabAPITests.xml");
    TestSuite testSuite = project.getTestSuiteByName("APITestSuite_1");
    TestCase testCase = testSuite.getTestCaseByName("[TC001] avatar TestCase");

    TestRunner runner = testCase.run(new PropertiesMap(), false);
    assertEquals(TestRunner.Status.FINISHED, runner.getStatus());
}
```

## Create new test case

The true advantage of this integration is possibility to create very new test cases right from java code. It gives us full control and flexibility.  
  
General handling of the request will look as follows:

```java
// prepare request
req = (RestRequest) resource.getRequestByName("RequestName");
req.setPropertyValue("PropName", "PropValue");
sub = req.submit(new WsdlSubmitContext(req), false);

// send it and collect response
resp = sub.getResponse();
stat = resp.getResponseHeaders().get("#status#", "");
```

For instance, simple test of project creation could look like this:

```java
import com.eviware.soapui.impl.rest.RestRequest;
import com.eviware.soapui.impl.support.AbstractInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlSubmitContext;
import com.eviware.soapui.model.iface.Operation;
import com.eviware.soapui.model.iface.Request;
import com.eviware.soapui.model.iface.Response;
import com.eviware.soapui.model.iface.Submit;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExampleAPITest {
    private static WsdlProject project;
    private static AbstractInterface resources;
    private Operation resource;
    private RestRequest req;
    private Submit sub;
    private Response resp;
    private enum StatusCode{
        s200("HTTP/1.1 200 OK"), s201("HTTP/1.1 201 Created"), s202("HTTP/1.1 202 Accepted");
        private final String status;
        StatusCode(String status){
            this.status = status;
        }
    }

    @BeforeAll
    public static void initData() throws Exception {
        String file = "../GitLabAPITests.xml";
        project = new WsdlProject(file);
        project.setPropertyValue("testUser", "JWMagisterka");
        project.setPropertyValue("testPass", "JWMgr2020");
        project.setPropertyValue("testEmail", "jacekwolny8@wp.pl");
        project.setPropertyValue("personalAccessToken", "K7rfQwtC8SSYdHs824pU");
        resources = project.getInterfaceByName("https://gitlab.com");
    }

    private String getJSONProp(Response resp, String property) {
        JSONObject contJSON;
        try {
            contJSON = new JSONObject(resp.getContentAsString());
            return contJSON.getString(property);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Test
    public void testProjectCreate() throws Request.SubmitException {
        // Arrange
        String pName = "JUnitProj",
                pDesc = "created via REST using JUnit (╯°□°）╯︵ ┻━┻",
                pVis = "public";
        String stat, actName;
        resource = resources.getOperationByName("projects");

        // Act; create project
        req = (RestRequest) resource.getRequestByName("PostProject");
        req.setPropertyValue("name", pName);
        req.setPropertyValue("description", pDesc);
        req.setPropertyValue("visibility", pVis);
        sub = req.submit(new WsdlSubmitContext(req), false);
        resp = sub.getResponse();
        actName = getJSONProp(resp, "name");
        stat = resp.getResponseHeaders().get("#status#", "");

        // Assert
        assertEquals(StatusCode.s201.status, stat);
        assertEquals(pName, actName);
    }
}
```

Check my sample project for more examples.