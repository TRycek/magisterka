import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestRunner;
import com.eviware.soapui.model.testsuite.TestSuite;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SingleSoapUITest {
    @Test
    public void TC001_avatarTestCase() throws Exception
    {
        WsdlProject project = new WsdlProject("../GitLabAPITests.xml");
        TestSuite testSuite = project.getTestSuiteByName("APITestSuite_1");
        TestCase testCase = testSuite.getTestCaseByName("[TC001] avatar TestCase");

        // create empty properties and run synchronously
        TestRunner runner = testCase.run(new PropertiesMap(), false);
        assertEquals(TestRunner.Status.FINISHED, runner.getStatus());
    }
}
