import static org.junit.jupiter.api.Assertions.assertEquals;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestRunner;
import com.eviware.soapui.model.testsuite.TestSuite;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SelectedSoapUITests {
    private static WsdlProject project;
    private static TestSuite testSuite;
    private static String[] testCases;
    private static int[] testCaseNumbers;

    @BeforeAll
    public static void initData() throws Exception {
        String file = System.getProperty("file") != null ? System.getProperty("file") : "../GitLabAPITests.xml";
        String suite = System.getProperty("suite") != null ? System.getProperty("suite") : "APITestSuite_1";
        project = new WsdlProject(file);
        testSuite = project.getTestSuiteByName(suite);

        testCases = new String[]{
                "[TC001] avatar TestCase",
                "[TC002] projects TestCase",
                "[TC003] project custom_attributes TestCase",
                "[TC004] project deploy_keys TestCase",
                "[TC005] deploy_keys TestCase",
                "[TC006] events TestCase",
                "[TC007] issues TestCase",
                "[TC008] issues advanced TestCase",
                "[TC009] issues notes TestCase",
                "[TC010] issues award_emoji TestCase",
                "[TC011] issues discussions TestCase",
                "[TC012] issues_statistics TestCase",
                "[TC013] merge_requests TestCase",
                "[TC014] merge_requests advanced TestCase",
                "[TC015] merge_requests commenting TestCase",
                "[TC016] notification_settings TestCase",
                "[TC017] pages/domains TestCase",
                "[TC018] users TestCase",
                "[TC019] fork TestCase",
                "[TC020] star TestCase",
                "[TC021] languages TestCase",
                "[TC022] archive TestCase",
                "[TC023] files TestCase",
                "[TC024] share TestCase",
                "[TC025] hooks TestCase",
                "[TC026] housekeeping TestCase",
                "[TC027] transfer TestCase",
                "[TC028] snapshot TestCase",
                "[TC029] runners TestCase",
                "[TC030] search TestCase",
                "[TC031] members TestCase",
                "[TC032] snippets TestCase",
                "[TC033] snippets comments TestCase",
                "[TC034] branches TestCase",
                "[TC035] commits TestCase",
                "[TC036] repository TestCase",
                "{TC037] deployments & envs TestCase",
                "[TC038] error_tracking settings TestCase",
                "[TC039] feature_flags TestCase",
                "[TC040] boards TestCase"
        };

        // define numbers of TCs to be executed
        testCaseNumbers = Arrays.stream(System.getProperty("tcs").split(",")).mapToInt(s -> Integer.parseInt(s)).toArray(); // from argument
        //testCaseNumbers = new int[] {-5, 0, 1, 8, 40, 999}; // specific numbers {1; 8; 40}
        //testCaseNumbers = IntStream.range(1, 11).toArray(); // range [1; 10]
        //testCaseNumbers = IntStream.range(1, 15).filter(i -> i < 3 || i > 5).filter(i -> i < 7 || i > 9).toArray(); // range with holes [1;2]u{6}u[10;14]
        //testCaseNumbers = IntStream.range(1, testCases.length + 1).toArray(); // all TCs
    }

    private static Stream provideTestNames(){
        return Arrays.stream(testCaseNumbers).filter(i -> (i <= testCases.length) && (i > 0)).mapToObj(i -> testCases[i-1]);
    }

    @ParameterizedTest
    @MethodSource("provideTestNames")
    public void runSelectedTests(String name)
    {
        TestCase testCase = testSuite.getTestCaseByName(name);

        TestRunner runner = testCase.run(new PropertiesMap(), false);
        assertEquals(TestRunner.Status.FINISHED, runner.getStatus());
    }
}
