import com.eviware.soapui.support.types.StringToStringsMap;
import com.eviware.soapui.impl.rest.RestRequest;
import com.eviware.soapui.impl.support.AbstractInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlSubmitContext;
import com.eviware.soapui.model.ModelItem;
import com.eviware.soapui.model.iface.Operation;
import com.eviware.soapui.model.iface.Request;
import com.eviware.soapui.model.iface.Response;
import com.eviware.soapui.model.iface.Submit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomTests {
    private static WsdlProject project;
    private static AbstractInterface resources;
    private Operation resource;
    private RestRequest req;
    private Submit sub;
    private Response resp;
    private enum StatusCode{
        s200("HTTP/1.1 200 OK"), s201("HTTP/1.1 201 Created"), s202("HTTP/1.1 202 Accepted");
        private final String status;
        StatusCode(String status){
            this.status = status;
        }
    }

    @BeforeAll
    public static void initData() throws Exception {
        String file = System.getProperty("file") != null ? System.getProperty("file") : "../GitLabAPITests.xml";
        project = new WsdlProject(file);
        project.setPropertyValue("testUser", "JWMagisterka");
        project.setPropertyValue("testPass", "JWMgr2020");
        project.setPropertyValue("testEmail", "jacekwolny8@wp.pl");
        project.setPropertyValue("personalAccessToken", "K7rfQwtC8SSYdHs824pU");
        resources = project.getInterfaceByName("https://gitlab.com");
    }

    private Operation getSubResource(Operation resource, String name){
        for (ModelItem child : resource.getChildren()) {
            if (child.getName().equals(name))
                return (Operation) child;
        }
        return null;
    }

    private String getJSONProp(Response resp, String property) {
        JSONObject contJSON;
        try {
            contJSON = new JSONObject(resp.getContentAsString());
            return contJSON.getString(property);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private int getJSONNodesCount(Response resp) {
        JSONArray contJSON;
        try {
            contJSON = new JSONArray(resp.getContentAsString());
            return contJSON.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Test
    public void basicProjectOperationsTest() throws Request.SubmitException {
        String pName = "JUnitProj",
                pDesc = "created via REST using JUnit (╯°□°）╯︵ ┻━┻",
                pVis = "public",
                pNewName = "JUnitProjUpdated";
        String stat, pID, actID, actName;
        int pCountBefore, pCountAfter;

        resource = resources.getOperationByName("projects");

        //// get projects
        req = (RestRequest) resource.getRequestByName("GetProjects");
        req.setPropertyValue("owned", "true");
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        pCountBefore = getJSONNodesCount(resp);
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(StatusCode.s200.status, stat);

        //// create project
        req = (RestRequest) resource.getRequestByName("PostProject");
        req.setPropertyValue("name", pName);
        req.setPropertyValue("description", pDesc);
        req.setPropertyValue("visibility", pVis);
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        pID = getJSONProp(resp, "id");
        actName = getJSONProp(resp, "name");
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(pName, actName);
        assertEquals(StatusCode.s201.status, stat);

        //// get projects
        req = (RestRequest) resource.getRequestByName("GetProjects");
        req.setPropertyValue("owned", "true");
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        pCountAfter = getJSONNodesCount(resp);
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(StatusCode.s200.status, stat);
        assertEquals(pCountBefore + 1, pCountAfter);

        resource = getSubResource(resource, "{projectId}");

        //// modify project
        req = (RestRequest) resource.getRequestByName("PutProject");
        req.setPropertyValue("projectId", pID);
        req.setPropertyValue("name", pNewName);
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        actID = getJSONProp(resp, "id");
        actName = getJSONProp(resp, "name");
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(pID, actID);
        assertEquals(pNewName, actName);
        assertEquals(StatusCode.s200.status, stat);

        //// get project
        req = (RestRequest) resource.getRequestByName("GetProject");
        req.setPropertyValue("projectId", pID);
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        actID = getJSONProp(resp, "id");
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(pID, actID);
        assertEquals(StatusCode.s200.status, stat);

        //// delete project
        req = (RestRequest) resource.getRequestByName("DelProject");
        req.setPropertyValue("projectId", pID);
        sub = req.submit(new WsdlSubmitContext(req), false);

        resp = sub.getResponse();
        stat = resp.getResponseHeaders().get("#status#", "");

        assertEquals(StatusCode.s202.status, stat);


        //String[] props = resp.getPropertyNames(); // check properties
        //StringToStringsMap headers = resp.getResponseHeaders(); // check headers
    }
}
