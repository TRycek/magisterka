import com.eviware.soapui.tools.SoapUITestCaseRunner;
import org.junit.jupiter.api.Test;

public class WholeSoapUITestSuite {
    @Test
    public void runAllTests() throws Exception
    {
        SoapUITestCaseRunner runner = new SoapUITestCaseRunner();
        runner.setProjectFile("../GitLabAPITests.xml");
        runner.run();
    }
}
